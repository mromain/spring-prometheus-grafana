package com.example.demo;

import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class ProductRoute {

    @Autowired
    private MeterRegistry registry;

    private int generateRandomInt() {
        Random r = new Random();
        return r.nextInt((100 - 1) + 1) + 1;
    }

    @PostMapping(value = "/api/product", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public int addProduct(@RequestBody Product product) {
        // Nb commandes par produit
        registry.counter("product.name." + product.getName()).increment();

        // Nb commandes par utilisateur
        registry.counter("user.name." + product.getUserFirstname() + " . " + product.getUserLastname()).increment();

        // Nb commandes par pays
        registry.counter("order.byCountry." + product.getCountry()).increment();

        // Nb commandes sur le site
        registry.counter("order.totalNumber").increment();

        return generateRandomInt();
    }
}
