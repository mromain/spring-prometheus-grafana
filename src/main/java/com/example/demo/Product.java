package com.example.demo;

public class Product {
    private String userFirstname;
    private String userLastname;
    private String address;
    private String country;
    private String name;

    public Product(String userFirstname, String userLastname, String address, String country, String name) {
        this.userFirstname = userFirstname;
        this.userLastname = userLastname;
        this.address = address;
        this.country = country;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
